<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabla de Gaseosas</title>
</head>
<body>

<?php
// Datos de las gaseosas
$gaseosas = array(
    array("Coca Cola", 100, number_format(4500, 0, ",", ".")),
    array("Pepsi", 30, number_format(4800, 0, ",", ".")),
    array("Sprite", 20, number_format(4500, 0, ",", ".")),
    array("Guarana", 200, number_format(4500, 0, ",", ".")),
    array("SevenUp", 24, number_format(4800, 0, ",", ".")),
    array("Mirinda Naranja", 56, number_format(4800, 0, ",", ".")),
    array("Mirinda Guarana", 89, number_format(4800, 0, ",", ".")),
    array("Fanta Naranja", 10, number_format(4500, 0, ",", ".")),
    array("Fanta Piña", 2, number_format(4500, 0, ",", "."))
);

?>

<table>
    <tr>
        <td colspan="3" id="titulo">Producto</td>
    </tr>
    <tr>
        <th>Nombre</th>
        <th>Cantidad</th>
        <th>Precio (Gs.)</th>
    </tr>

    <?php
        //Definimos el estilo de la tabla
        echo '<style>
        table {
            border-collapse: collapse;
            
        }

        th, td {
            border: 1px solid black;
        }

        th {
            background-color: #bebebe;
        }

        #titulo {
            background-color: yellow;
            text-align: center;
        }

        tr:nth-child(odd) {
            background-color: #C6D5DC;
        }
        </style>';

    // Recorremos el array e imprimimos los datos
    foreach ($gaseosas as $gaseosa) {
        echo "<tr>";
        echo "<td>{$gaseosa[0]}</td>";
        echo "<td style='text-align: center;'>{$gaseosa[1]}</td>";
        echo "<td style='text-align: center;'>{$gaseosa[2]}</td>";
        echo "</tr>";
    }
    ?>

</table>

</body>
</html>
